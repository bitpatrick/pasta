package it.prismaprogetti.academy.run;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.prismaprogetti.academy.PastaReader;
import it.prismaprogetti.academy.PastaWriter;
import it.prismaprogetti.academy.impl.DataRecordPastaReader;
import it.prismaprogetti.academy.impl.DataRecordPastaWriter;
import it.prismaprogetti.academy.pastamodel.Codice;
import it.prismaprogetti.academy.pastamodel.CodiceMarca;
import it.prismaprogetti.academy.pastamodel.Descrizione;
import it.prismaprogetti.academy.pastamodel.Nome;
import it.prismaprogetti.academy.pastamodel.Pasta;
import it.prismaprogetti.academy.pastamodel.Prezzo;

public class Main {

	public static void main(String[] args) throws IOException {

		String path = "C:\\Users\\patri\\eclipse-workspace\\Pasta\\src\\main\\resources\\data.txt";
		
		/*
		 * creo dati da scrivere
		 */
		Pasta pastaBarilla1 = Pasta.creaOrNull(Codice.createOrNull("B001"), Nome.createOrNull("Spaghetti Barilla 500g"), Descrizione.createOrNull("Confezione spaghetti di grano leggero pasta Barilla di 500g"), Prezzo.createOrNull("1,30"), CodiceMarca.createOrNull("BARILLA"));
		Pasta pastaBarilla2 = Pasta.creaOrNull(Codice.createOrNull("B002"), Nome.createOrNull("Rigatoni Barilla 500g"), Descrizione.createOrNull("Confezione rigatoni di grano leggero pasta Barilla di 500g"), Prezzo.createOrNull("1,00"), CodiceMarca.createOrNull("BARILLA"));
		Pasta pastaBarilla3 = Pasta.creaOrNull(Codice.createOrNull("B003"), Nome.createOrNull("Fusilli Barilla 250g"), Descrizione.createOrNull("Confezione fusilli di grano leggero pasta Barilla di 250g"), Prezzo.createOrNull("1,00"), CodiceMarca.createOrNull("BARILLA"));
		Pasta pastaBarilla4 = Pasta.creaOrNull(Codice.createOrNull("B004"), Nome.createOrNull("Mezze maniche Barilla 1kg"), Descrizione.createOrNull("Confezione mezze maniche di grano leggero pasta Barilla di 1kg"), Prezzo.createOrNull("2,00"), CodiceMarca.createOrNull("BARILLA"));
	
		
		
		/*
		 * creo una lista dove inserire i suddetti dati
		 */
		List<Pasta> pastaList = new ArrayList<>();
		pastaList.add(pastaBarilla1);
		pastaList.add(pastaBarilla2);
		pastaList.add(pastaBarilla3);
		pastaList.add(pastaBarilla4);
		
		/*
		 * richiamo il manager di scrittura
		 */
		PastaWriter pastaWriter = new DataRecordPastaWriter('*');		
		
		/*
		 * creo il file di testo e vi inserisco i suddetti dati
		 */
		pastaWriter.write(pastaList, path);
		
		/*
		 * leggo il file sorgente
		 */
		PastaReader pastaReader = new DataRecordPastaReader('*');
		
		pastaReader.read(path);
		
	
	}

}
