package it.prismaprogetti.academy.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import it.prismaprogetti.academy.PastaReader;
import it.prismaprogetti.academy.pastamodel.Codice;
import it.prismaprogetti.academy.pastamodel.CodiceMarca;
import it.prismaprogetti.academy.pastamodel.Descrizione;
import it.prismaprogetti.academy.pastamodel.Nome;
import it.prismaprogetti.academy.pastamodel.Pasta;
import it.prismaprogetti.academy.pastamodel.Prezzo;

public class DataRecordPastaReader implements PastaReader {

	private final char marcatore;

	public DataRecordPastaReader(char marcatore) {
		super();
		this.marcatore = marcatore;
	}

	@Override
	public void read(String path) throws IOException {

		try (Reader reader = new InputStreamReader(new FileInputStream(path));
				BufferedReader bufferedReader = new BufferedReader(reader);

		) {

			String intestazioneString = bufferedReader.readLine(); // CODICE,NOME,DESCRIZIONE,PREZZO,CODICE_MARCA
			String[] intestazioneArray = intestazioneString.split(","); // [CODICE|NOME|DESCRIZIONE|PREZZO|CODICE_MARCA]

			String datiString = "";

			while ( true) {

				String datoString = bufferedReader.readLine(); // B001 --> Spaghetti Barilla 500g --> Conf.. --> 1,30
																// --> BARILLA

				if (datoString == null) {
					break;
				}
				
				/*
				 * verifica se � la fine del record
				 */
				if (datoString.contains(String.valueOf(marcatore))) {

					/*
					 * creo array di dati
					 */
					String[] datiArray = datiString.split("-");

					/*
					 * creo attributi oggetto Pasta
					 */
					Codice codice = Codice.createOrNull(datiArray[0]);
					Nome nome = Nome.createOrNull(datiArray[1]);
					Descrizione descrizione = Descrizione.createOrNull(datiArray[2]);
					Prezzo prezzo = Prezzo.createOrNull(datiArray[3]);
					CodiceMarca codiceMarca = CodiceMarca.createOrNull(datiArray[4]);

					/*
					 * creo oggetto Pasta
					 */
					Pasta pasta = Pasta.creaOrNull(codice, nome, descrizione, prezzo, codiceMarca);

					/*
					 * stampo oggetto pasta
					 */
					System.out.println(pasta.toString());

					/*
					 * clear datiString
					 */
					datiString = "";

					/*
					 * passo al prossimo dato
					 */
					continue;
				}

				datiString += datoString + "-";

			}

		}

	}

}
