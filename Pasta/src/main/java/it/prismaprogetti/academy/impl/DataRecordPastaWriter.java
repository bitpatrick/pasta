package it.prismaprogetti.academy.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import it.prismaprogetti.academy.PastaWriter;
import it.prismaprogetti.academy.pastamodel.Pasta;

/**
 * componente di scrittura,ovvero quello che crea il file testo
 * 
 * @author patri
 *
 */
public class DataRecordPastaWriter implements PastaWriter {

	private final char marcatore;

	public DataRecordPastaWriter(char marcatore) {
		super();
		this.marcatore = marcatore;
	}

	@Override
	public void write(List<Pasta> pastaList, String path) throws IOException {

		try (Writer outputStreamWriter = new OutputStreamWriter(new FileOutputStream(path));
				BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);) {

			/*
			 * inserimento intestazione nel file
			 */
			bufferedWriter.write("CODICE, NOME, DESCRIZIONE, PREZZO, CODICE_MARCA\n");

			/*
			 * inserimento dati del prodotto pasta nel file
			 */
			for (Pasta pasta : pastaList) {

				bufferedWriter.write(pasta.getCodice().getCodice() + "\n");
				bufferedWriter.write(pasta.getNome().getNome() + "\n");
				bufferedWriter.write(pasta.getDescrizione().getDescrizione() + "\n");
				bufferedWriter.write(pasta.getPrezzo().getPrezzo() + "\n");
				bufferedWriter.write(pasta.getCodiceMarca().getCodice() + "\n");

				/*
				 * inserimento marcatore fine prodotto
				 */
				for (int i = 0; i < 3; i++) {
					bufferedWriter.write(marcatore);
				}
				bufferedWriter.write("\n");

			}

		}

	}

}
