package it.prismaprogetti.academy;

import java.io.IOException;

public interface PastaReader {
	
	public void read(String path) throws IOException;

}
