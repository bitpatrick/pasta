package it.prismaprogetti.academy;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import it.prismaprogetti.academy.pastamodel.Pasta;

public interface PastaWriter {
	
	
	public void write(List<Pasta> pastaList, String path) throws IOException;

}
