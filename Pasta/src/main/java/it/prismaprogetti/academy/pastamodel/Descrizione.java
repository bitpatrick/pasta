package it.prismaprogetti.academy.pastamodel;

public class Descrizione {
	
	private String descrizione;

	private Descrizione(String descrizione) {
		super();
		this.descrizione = descrizione;
	}

	public String getDescrizione() {
		return descrizione;
	}
	
	public static Descrizione createOrNull(String descrizione) {

		return new Descrizione(NullManager.createNullStringIsNecessarily(descrizione));
	}

}
