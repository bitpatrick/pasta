package it.prismaprogetti.academy.pastamodel;

public class Nome {

	private String nome;

	private Nome(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
	
	public static Nome createOrNull(String nome) {

		return new Nome(NullManager.createNullStringIsNecessarily(nome));
	}

}
