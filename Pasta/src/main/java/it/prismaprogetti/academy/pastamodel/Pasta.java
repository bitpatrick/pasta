package it.prismaprogetti.academy.pastamodel;

public class Pasta {

	private Codice codice;
	private Nome nome;
	private Descrizione descrizione;
	private Prezzo prezzo;
	private CodiceMarca codiceMarca;
	private static final String identificatoreDiNullita = "<<NULL>>";

	private Pasta(Codice codice, Nome nome, Descrizione descrizione, Prezzo prezzo, CodiceMarca codiceMarca) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.descrizione = descrizione;
		this.prezzo = prezzo;
		this.codiceMarca = codiceMarca;
	}

	public static Pasta creaOrNull(Codice codice, Nome nome, Descrizione descrizione, Prezzo prezzo,
			CodiceMarca codiceMarca) {

		return new Pasta(codice, nome, descrizione, prezzo, codiceMarca);
	}

	public Codice getCodice() {
		return codice;
	}

	public Nome getNome() {
		return nome;
	}

	public Descrizione getDescrizione() {
		return descrizione;
	}

	public Prezzo getPrezzo() {
		return prezzo;
	}

	public CodiceMarca getCodiceMarca() {
		return codiceMarca;
	}

	public static String getIdentificatoredinullita() {
		return identificatoreDiNullita;
	}

	@Override
	public int hashCode() {
		
		return (this.codice.getCodice() + this.codiceMarca.getCodice()).hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if ( obj == null ) {
			return false;
		}
		if ( obj instanceof Pasta) {
			Pasta pastaObj = (Pasta) obj;
			return pastaObj.equals(this);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Pasta [codice=" + codice.getCodice() + ", nome=" + nome.getNome() + ", descrizione=" + descrizione.getDescrizione() + ", prezzo=" + prezzo.getPrezzo()
				+ ", codiceMarca=" + codiceMarca.getCodice() + "]";
	}
	
}
