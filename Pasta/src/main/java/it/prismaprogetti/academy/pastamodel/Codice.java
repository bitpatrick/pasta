package it.prismaprogetti.academy.pastamodel;

public class Codice {

	private String codice;
	

	private Codice(String codice) {
		super();
		this.codice = codice;
	}

	public String getCodice() {
		return codice;
	}

	public static Codice createOrNull(String codice) {
		
		return new Codice(NullManager.createNullStringIsNecessarily(codice));
	}

}
