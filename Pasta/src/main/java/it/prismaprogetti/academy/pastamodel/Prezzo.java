package it.prismaprogetti.academy.pastamodel;

public class Prezzo {
	
	private String prezzo;

	private Prezzo(String prezzo) {
		super();
		this.prezzo = prezzo;
	}

	public String getPrezzo() {
		return prezzo;
	}
	
	public static Prezzo createOrNull(String prezzo) {

		return new Prezzo(NullManager.createNullStringIsNecessarily(prezzo));
	}

}
