package it.prismaprogetti.academy.pastamodel;

public class NullManager {
	
	public static String createNullStringIsNecessarily( String param ) {
		
		if ( param.isBlank() ) {
			return "<<NULL>>";
		}
		
		return param;
	}

}
