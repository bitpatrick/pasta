package it.prismaprogetti.academy.pastamodel;

public class CodiceMarca {
	
	private String codice;

	private CodiceMarca(String codice) {
		super();
		this.codice = codice;
	}

	public String getCodice() {
		return codice;
	}
	
	public static CodiceMarca createOrNull(String codice) {

		return new CodiceMarca(NullManager.createNullStringIsNecessarily(codice));
	}

}
